#!/bin/sh

fzflike=fzf

posturl=$1
candidateurl=$2
keyurl=$3
code=$4

encrypt() { # $1 = unencrypted $2 = keyfile
	unencryptedBase64="$(echo -n "$1" | base64)"
	encryptedBase64="$(echo -n "$unencryptedBase64" | openssl rsautl -pubin -inkey "$2" -encrypt | base64)"
	echo -n "$encryptedBase64"
}

if [[ $code == "" ]]
then
	echo "usage: ./client.sh posturl candidateurl code"
else
	candidates="$(curl --silent $candidateurl)"
	# vote="$(echo -e "done\n$candidates" | $fzflike)"
	# votes="$vote"
	while [[ $vote != "done" ]]
	do
		vote="$(echo -e -n "done\n$candidates" | $fzflike)"
		if [[ $vote != "done" ]]
		then
			if [[ $votes == "" ]]
			then
				votes="$vote"
			else
				votes="$votes,$vote"
			fi
			candidates="$(echo -n "$candidates" | sed "s/^$vote$//" | grep .)"
		fi
	done
	if [[ $votes != "" ]]
	then
		promptres="$(echo -e "yes\nno\nyour vote looks like this: $votes. do you want to send it?" | $fzflike)"
		if [[ $promptres == "yes" ]]
		then
			tempfile="$(mktemp /tmp/ota-ääni-client.XXXXXX)"
			curl --silent "$keyurl" > "$tempfile"
			votes="$(encrypt "$votes" "$tempfile")"
			echo -n "$votes" > file1
			rm "$tempfile"
		wget --post-data="code=$code&vote=$votes" "$posturl" --quiet --output-document=-
		fi
	fi
fi
