# Ota-äänet

otaniemen lukion nettiäänestysjärjestelmä

## Tarkoitus

Tämän ohjelman tarkoituksena on mahdollistaa äänestäminen internetin välityksellä
oppilaskunnanhallituksen vaaleissa.

## Toimintaperiaate

järjestelmä perustuu digitaaliseen allekirjoitukseen ja satunnaisiin, vain 
äänestäjälle tiedossa oleviin, koodeihin.

### Äänestyksen kulku

1. käyttäjä saa koodin, jolla voi tunnistautua palveluun
2. käyttäjä lisää jokaisen ehdokkaan koodin perään saman satunnaisen luvun
3. käyttäjä lähettää näin saadut äänet koodit piilotettuina palvelimelle
   saamansa tunnistautumiskoodin kanssa.
4. palvelin allekirjoittaa käytäjän äänet ja palauttaa ne
5. käyttäjä purkaa piilotuksen äänien koodeista ja lähettää näistä äänistä
   haluamansa määrän haluamassansa järjestyksessä palvelimelle laskettaviksi
6. palvelin julkistaa lasketuksi lähetetyt äänet, jolloin käyttäjä voi tarkistaa,
   että hänen äänensä on mennyt läpi.
7. julkistetut äänet lasketaan palvelimella ja tulos julkistetaan. Kuka tahansa
   voi tarkistaa että tulos on oikein käyttämällä samaa ääntenlaskumenetelmää.

## Lisenssi

Tämä ohjelma julkaistaan GNU AGPL lisenssin version 3 tai myöhemmän ehdoilla.
Saat siis päättää mitä näistä versioista noudatat.
