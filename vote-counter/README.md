Käyttö:
* Kopio palvelimelta votes.txt, candidates.txt ja pub-key.pem
* Aja `python count.py`
* Tulokset: lopussa FINAL RESULT kohdassa on lopputulokset
* Jos haluaa suosion ensimmäisten äänien kohdalla, katso alusta ROUND 1
* PJ-tulokset: katso lopusta kenellä on eniten ääniä