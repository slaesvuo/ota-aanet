import json
import random
with open('candidates.txt') as f:
    candidates = f.readlines()

candidates = [x.strip() for x in candidates] 

votes = []

n = int(input("How many votes to gen: "))

for i in range(n):
    tokens = ','.join(random.sample(candidates, 3))
    pjvote = "25 Jussi Valjus 19M (pj)"
    randomid = random.randint(0,10000000000)
    votes.append(str(randomid)+"-"+tokens+"-"+pjvote+'-12345fakesig\n')

votesoutfile = open('votes.txt', 'w')
votesoutfile.writelines(votes)

votesoutfile.close()