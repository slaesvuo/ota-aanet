import pyrankvote
from pyrankvote import Candidate, Ballot
import json
from hashlib import sha256
from rsa import PublicKey

ballotsfile = open('votes.txt')
candidatesfile = open('candidates.txt', 'r')

seatcount = int(input('How many candidates are chosen: '))

pjvotes = {}
countedids = []
ballots = []
candidates = {}
with open('pub-key.pem', 'r') as f:
    public_key = PublicKey.load_pkcs1(f.read(), "PEM")
    f.close()

n = public_key.n
e = public_key.e

def verify_sig(message, sig):
    global n, e
    message = message.encode("utf-8")
    sig = int(sig)

    hashed = sha256(message).hexdigest()

    original_hash = pow(sig, e, n)
    original_hash = format(original_hash, 'x')

    return hashed == original_hash
    

for candidate in candidatesfile:
    candidate = candidate.strip()
    candidates[candidate] = Candidate(candidate)

for ballot in ballotsfile:
    votelist = []
    ballotparts = ballot.split('-')
    ballotid = ballotparts[0]
    baseballot = f"{ballotparts[0]}-{ballotparts[1]}-{ballotparts[2]}"
    if ballotid in countedids:
        print("Skipped due to ballotid already existing: "+ballot)
        continue
    if not verify_sig(baseballot, ballotparts[3]):
        print("Invalid signature: "+ballot)
        continue
    
    countedids.append(ballotid)

    pjvote = ballotparts[2]
    for i, vote in enumerate(ballotparts[1].split(',')):
        if i >= 3:
            print("Ignoring remaining votes on, over 3 votes in ballot: "+ballot)
            break
        vote = vote.strip()
        if vote in candidates:
            votelist.append(candidates[vote])
        else:
            print("Invalid vote: "+vote)
    ballots.append(Ballot(votelist))
    if pjvote in pjvotes:
        pjvotes[pjvote] += 1
    else:
        pjvotes[pjvote] = 1

result = pyrankvote.single_transferable_vote(candidates.values(), ballots, seatcount)

winners = result.get_winners()

print("----- STV RESULTS ------")
print(result)

print("----- PJ RESULTS ------")
print(pjvotes)