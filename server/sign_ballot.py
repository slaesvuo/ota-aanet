from read import read_file
from base64 import b64encode, b64decode
from rsa import PrivateKey
import config

private_key = PrivateKey.load_pkcs1(read_file(config.priv_key_file), "PEM")
n = private_key.n
d = private_key.d


valid_codes = []
used_codes = []
used_codes_handle = open(config.used_codes_file, 'a')

def sign_ballot(code, blinded):
    global d, n, valid_codes
    if code in valid_codes:
        blinded = blinded.encode("ascii")
        blinded = b64decode(blinded)
        blinded = blinded.decode("ascii")
        blinded = int(blinded)
    
        signature = sign_ballot_encoded(blinded, d, n)

        use_code(code)
        return {'signature': signature}
    else:
        print("INVALID CODE!!!")
        print(f"    code: {code}")
        print(f"    blinded: {blinded}")
        return {'error': 'Invalid code'}, 403

def load_codes():
    global valid_codes, used_codes
    with open(config.codes_file, 'r') as f:
        valid_codes = [line.rstrip() for line in f]
    with open(config.used_codes_file, 'r') as f:
        used_codes = [line.rstrip() for line in f]
    valid_codes = [code for code in valid_codes if code not in used_codes]

def use_code(code):
    global valid_codes, used_codes, used_codes_handle
    used_codes.append(code)
    valid_codes.remove(code)
    used_codes_handle.write(f"{code}\n")


def do_sign_ballot(blinded, d, n):
    signature = pow(blinded, d, n)
    return signature

def sign_ballot_encoded(blinded, d, n):
    signature_bytes = str(do_sign_ballot(blinded, d, n)).encode("ascii")
    return b64encode(signature_bytes).decode("ascii")

load_codes()