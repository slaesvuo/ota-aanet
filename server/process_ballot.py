# Copyright (C) Saku Laesvuori
#
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# type Code = String
# type Filename = String
# type ballot = String

from read import read_file
from hashlib import sha256
from rsa import PrivateKey
import re
import config

private_key = PrivateKey.load_pkcs1(read_file(config.priv_key_file), "PEM")
n = private_key.n
e = private_key.e

def process_ballot(ballot, ballot_file): # :: ballot -> Filename -> IO ()
    (token, sig) = parse_ballot(ballot)
    is_verified = verify_sig(token, sig)
    if is_verified:
        write_ballot(ballot, ballot_file)
        return {'ballot': ballot}
    else:
        return {'error': 'Invalid signature'}, 400

def parse_ballot(ballot):
    splitted = ballot.split('-')
    token = f"{splitted[0]}-{splitted[1]}-{splitted[2]}"
    sig = splitted[3]
    return (token, sig)

def update_codes(code, codes, codes_file): # :: Code -> [Code] -> Filename -> IO ()
    codes.remove(code)
    code_file_handle = open(codes_file, "w")
    for code in codes:
        code_file_handle.write(code+"\n")
    code_file_handle.close()

def verify_sig(message, sig):
    global n, e
    message = message.encode("utf-8")
    sig = int(sig)

    hashed = sha256(message).hexdigest()

    original_hash = pow(sig, e, n)
    original_hash = format(original_hash, 'x')

    print(original_hash)

    return hashed == original_hash

def read_codes(codes_file): # :: Filename -> IO [Code]
    valid_codes = read_file(codes_file)
    valid_codes = valid_codes.split("\n")
    return valid_codes

def write_ballot(ballot, ballot_file): # :: ballot -> Filename -> IO ()
    ballot_file = open(ballot_file, "a")
    ballot_file.write(ballot+"\n")
    ballot_file.close()
