# Copyright (C) Saku Laesvuori
#
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

from flask import Flask, request
from flask_cors import CORS, cross_origin
from process_ballot import process_ballot
from sign_ballot import sign_ballot
import config
from read import read_file

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/candidates")
@cross_origin()
def candidates():
    candidate_list = read_file(config.candidate_file)
    return candidate_list

@app.route("/pub-key")
@cross_origin()
def pub_key():
    key = read_file(config.pub_key_file)
    return key

@app.route("/sign-ballot", methods=["POST"])
@cross_origin()
def sign_received_ballot():
    code = request.form["code"]
    ballot = request.form["ballot"]
    return sign_ballot(code, ballot)
    

@app.route("/send-ballot", methods=["POST"])
@cross_origin()
def receive_ballot():
    ballot = request.form["ballot"]
    return process_ballot(ballot, config.ballot_file)

if __name__ == "__main__":
    app.run()
