import smtplib

gmail_user = input('Gmail address: ')
gmail_password = input('Gmail password: ')

emails = open('emails.txt', 'r')
codes = open('codes.txt', 'a')

sent_from = gmail_user
subject = 'Otaniemen lukion opiskelijakunnan hallituksen äänestys'
body = 'Alla koodisi: \n'

def get_email_text(to, code):
    global sent_from, subject, body
    email_text = """\
    From: %s
    To: %s
    Subject: %s

    %s
    %s
    """ % (sent_from, to, subject, body, code)
    return email_text

try:
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(gmail_user, gmail_password)
    for to in emails:
        code = "1234"
        email_text = get_email_text(to, code)
        server.sendmail(sent_from, to, email_text)
        codes.write(f"{code}\n")
    server.close()

    print('Email sent!')
except Exception as error:
    print(error)
    print('Something went wrong...')