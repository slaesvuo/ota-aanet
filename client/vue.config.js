module.exports = {
  chainWebpack: config => {
    // PEM raw loader
    config.module
      .rule('pem-raw-loader')
      .test(/\.pem$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
    config.optimization.minimize(true)
  },
  configureWebpack: config => {
    config.mode = 'production'
    config.optimization.usedExports = true
  }
}
