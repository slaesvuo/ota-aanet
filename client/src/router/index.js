// Copyright (C) Juuso Laine 2020
//
// This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import VueRouter from 'vue-router'
import { hasVoted } from '@/voting'
import { store } from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/voted',
    name: 'Voted',
    component: () => import(/* webpackChunkName: "voted" */ '../views/Voted.vue')
  },
  {
    path: '/votingerror',
    name: 'VotingError',
    component: () => import(/* webpackChunkName: "votingerror" */ '../views/VotingError.vue')
  },
  {
    path: '/',
    name: 'Vote',
    component: () => import(/* webpackChunkName: "vote" */ '../views/Vote.vue'),
    beforeEnter: (to, from, next) => {
      if (to.query.code) {
        store.code = to.query.code
      }
      if (to.query.to) {
        next(to.query.to) // workaround for history and gitlab pages
      } else {
        if (hasVoted()) {
          next('/voted')
        } else {
          next()
        }
      }
    }
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
