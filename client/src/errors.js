class VotingError extends Error {

}

class InvalidSignature extends VotingError {

}

class Unauthorized extends VotingError {

}

class OtherError extends VotingError {

}

export default { InvalidSignature, Unauthorized, OtherError, VotingError }
