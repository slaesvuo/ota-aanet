import { pem2jwk, base64url2bn } from '@jusola/pem-jwk'
import BlindSignature from '@jusola/blind-signatures'
import secureRandom from 'secure-random'
import { Base64 } from 'js-base64'
import { store, storeInit } from './store'
import errors from '@/errors'
import { ToastProgrammatic as Toast } from 'buefy'

const signServerURL = process.env.VUE_APP_SIGNING_SERVER
const voteServerURL = process.env.VUE_APP_VOTING_SERVER

const signPublicKey = getPublicKey()

async function blind (message) {
  // blind
  var { blinded, r } = BlindSignature.blind({ // this blinds and hashes
    message: message,
    N: (await signPublicKey).n,
    E: (await signPublicKey).e
  })
  blinded = blinded.toString()
  r = r.toString()
  return { blinded, r }
}

// unblind signature
async function unblind (blindSignature, r) {
  const sig = BlindSignature.unblind({
    signed: blindSignature,
    N: (await signPublicKey).n,
    r: r
  })
  return sig.toString()
}

// verify signature for message
async function verify (message, sig) {
  return BlindSignature.verify({
    unblinded: sig,
    N: (await signPublicKey).n,
    E: (await signPublicKey).e,
    message: message
  }).toString()
}

async function submitBallot (ballot) {
  var formData = new URLSearchParams()
  formData.append('ballot', ballot)
  // do actual request
  const res = await fetch(`${voteServerURL}/send-ballot`, {
    method: 'POST',
    body: formData
  })
  const data = await res.json()
  if (!res.ok) {
    if (res.status === 400) {
      throw new errors.InvalidSignature(data.error)
    } else {
      throw new errors.OtherError(`Error while submtting ballot: ${res.status}`)
    }
  }
}

async function getSignatureFromServer (hashedBallot) {
  // construct http request body
  var formData = new URLSearchParams()
  formData.append('ballot', hashedBallot)
  formData.append('code', store.code)

  // do the actual request
  const res = await fetch(`${signServerURL}/sign-ballot`, {
    method: 'POST',
    body: formData
  })
  const data = await res.json()
  if (!res.ok) {
    if (res.status === 403) {
      throw new errors.Unauthorized(data.error)
    } else {
      throw new errors.OtherError(`Error while signing: ${res.status}`)
    }
  } else {
    return data.signature
  }
}

async function signBallot (blinded, r) {
  blinded = Base64.encode(blinded) // encode for server as b64

  let blindSig = await getSignatureFromServer(blinded) // get sig from server

  blindSig = Base64.decode(blindSig) // decode from b64

  // unblind sig
  const sig = await unblind(blindSig, r)
  return sig
}

function getRandomID (bytes) {
  const arr = secureRandom(bytes, { type: 'Uint8Array' })
  return Base64.fromUint8Array(arr, false)
}

async function getBallot (voteArray, pjvote, id) {
  const baseBallot = `${id}-${voteArray.join(',')}-${pjvote}`
  const { blinded, r } = await blind(baseBallot) // blind ballot
  const sig = await signBallot(blinded, r) // sign ballot on server

  store.backup = `${baseBallot}-${sig}`

  const isVerified = await verify(baseBallot, sig) // verify sig
  if (!isVerified) throw new errors.InvalidSignature('client side verification of ballot failed')

  return `${baseBallot}-${sig}`
}

async function vote () {
  try {
    const voteArray = store.votes
    const pjvote = store.pjvote
    store.errors = []
    const id = getRandomID(16)
    const ballot = await getBallot(voteArray, pjvote, id)
    await submitBallot(ballot)
    saveBallot(ballot)
    voteSuccess()
    return true
  } catch (error) {
    store.errors.push(error)
    if (error instanceof errors.InvalidSignature) {
      displayError('Virhe lipun allekirjoituksen validoinnissa')
      console.error('Signature error while voting: ', error.message)
      throw error
    } else if (error instanceof errors.Unauthorized) {
      displayError('Väärä koodi, ethän ole äänestänyt aiemmin?')
      console.warn('Invalid code: ', error.message)
    } else {
      displayError('Virhe äänestyksessä')
      console.error('Error while voting', error)
      throw error
    }
    return false
  }
}

function displayError (errorMessage) {
  Toast.open({
    message: errorMessage,
    type: 'is-danger',
    duration: 5000
  })
}

function voteSuccess () {
  Toast.open({
    message: 'Äänestäminen onnistui',
    type: 'is-success',
    duration: 5000
  })
}

async function getPublicKey () {
  const res = await fetch(`${signServerURL}/pub-key`, {
    method: 'GET'
  })
  const data = await res.text()
  if (res.ok) {
    return getPublicKeyFromPEM(data)
  }
}

async function getCandidates () {
  const res = await fetch(`${voteServerURL}/candidates`, {
    method: 'GET'
  })
  const data = await res.text()
  if (res.ok) {
    return data.split('\n').filter((val, ind, arr) => val !== "");
  }
}

async function init () {
  try {
    const candidates = await getCandidates()
    await storeInit(candidates)
  } catch (error) {
    console.error('error')
    displayError('Virhe lataamisessa taustapalvelimelta')
  }
}

function getPublicKeyFromPEM (pem) {
  const jwk = pem2jwk(pem)
  const n = base64url2bn(jwk.n).toString()
  const e = base64url2bn(jwk.e).toString()
  return { n, e }
}

function saveBallot (ballot) {
  if (getSavedBallot() === null) {
    localStorage.setItem('ballot', ballot)
  } else {
    console.error('Coudln\'nt save ballot - another ballot is already saved')
    displayError('Äänestyslippua ei voitu varmuuskopioida lokaalisti')
  }
}

function getSavedBallot () {
  return localStorage.getItem('ballot')
}

function deleteSavedBallot () {
  return localStorage.removeItem('ballot')
}

function hasVoted () {
  const savedBallot = getSavedBallot()
  return savedBallot !== null
}

init()

export { getBallot, vote, getRandomID, hasVoted, getSavedBallot, deleteSavedBallot }
