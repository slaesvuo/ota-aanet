var store = {
  votes: [],
  unvotedCandidates: [],
  allCandidates: [],
  code: null,
  errors: []
}

async function init (candidates) {
  store.unvotedCandidates = [...candidates]
  store.allCandidates = [...candidates]
}

export { store, init as storeInit }
